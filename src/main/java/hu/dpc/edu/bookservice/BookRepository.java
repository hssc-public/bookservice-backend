package hu.dpc.edu.bookservice;

import org.springframework.stereotype.Repository;

import java.util.*;
import java.util.stream.Collectors;

@Repository
public class BookRepository {
    private Map<String,Book> bookByIsbnMap = new HashMap<>();


    public String save(Book book) {
        Objects.requireNonNull(book, "book is required");
        Objects.requireNonNull(book.getIsbn(), "book.isbn is required");
        Objects.requireNonNull(book.getAuthor(), "book.author is required");
        Objects.requireNonNull(book.getTitle(), "book.title is required");
        if (book.getAuthor().length() == 0) {
            throw new NullPointerException("book.author is required");
        }
        if (book.getTitle().length() == 0) {
            throw new NullPointerException("book.title is required");
        }
        if (book.getIsbn().length() == 0) {
            throw new NullPointerException("book.isbn is required");
        }
        bookByIsbnMap.put(book.getIsbn(), book);
        return book.getIsbn();
    }

    public List<Book> findAll() {
        return this.bookByIsbnMap
                .values()
                .stream()
                .collect(Collectors.toList());
    }

    public Optional<Book>findByIsbn(String isbn) {
        return Optional.ofNullable(this.bookByIsbnMap.get(isbn));
    }

}
