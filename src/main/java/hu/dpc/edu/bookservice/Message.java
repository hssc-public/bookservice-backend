package hu.dpc.edu.bookservice;

public class Message {
    private String code;
    private String details;

    public Message(String code, String details) {
        this.code = code;
        this.details = details;
    }

    public String getCode() {
        return code;
    }

    public String getDetails() {
        return details;
    }
}
