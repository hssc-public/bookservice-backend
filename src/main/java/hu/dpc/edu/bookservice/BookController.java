package hu.dpc.edu.bookservice;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping(path="/books", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public class BookController {

    private BookRepository bookRepository;

    public BookController(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public void createBook(@RequestBody Book book) {
        this.bookRepository.save(book);
    }

    @GetMapping()
    public List<Book> findAll() {
        return this.bookRepository.findAll();
    }

    @GetMapping("/{isbn:\\w+}")
    public Book findByIsbn(@PathVariable String isbn) {
        return this.bookRepository.
                findByIsbn(isbn).
                orElseThrow(() -> new EntityNotFoundException("No book with ISBN: " + isbn));
    }

    @ExceptionHandler(EntityNotFoundException.class)
    public ResponseEntity<Message> handleEntityNotFoundException(EntityNotFoundException ex) {
        return ResponseEntity
                .status(HttpStatus.NOT_FOUND)
                .body(new Message("ENTITY_NOT_FOUND", ex.getMessage()));
    }

    @ExceptionHandler(NullPointerException.class)
    public ResponseEntity<Message> handleNullPointerException(NullPointerException ex) {
        return ResponseEntity
                .status(HttpStatus.UNPROCESSABLE_ENTITY)
                .body(new Message("INVALID_BOOK", ex.getMessage()));
    }
}
